package com.kosign.rotateanimation;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    ImageView iv_image;
    View v_top,v_bottom;
    TextView tv_text;
    EditText edt_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iv_image = findViewById(R.id.iv_image);
        v_top = findViewById(R.id.view_top);
        v_bottom = findViewById(R.id.view_bottom);
        tv_text = findViewById(R.id.tv_text);
        edt_text = findViewById(R.id.edt_text);
        edt_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                    }
                }else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(edt_text.getWindowToken(),0);
                    }
                }
            }
        });
        tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleAddRemove(true);
                animateLine(true);
                v.setVisibility(View.GONE);
                edt_text.setVisibility(View.VISIBLE);
                edt_text.requestFocus();
            }
        });
        iv_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleAddRemove(false);
                animateLine(false);

                edt_text.setVisibility(View.GONE);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                tv_text.setVisibility(View.VISIBLE);
            }
        });
    }

    private void animateLine(boolean startStop) {
        ResizeWidthAnimation animation;
        ResizeWidthAnimation animation2;
        if(startStop){
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) v_top.getLayoutParams();

            int layoutFullWidth = displayMetrics.widthPixels;
            int marginOfLines = lp.leftMargin + lp.rightMargin;

            animation = new ResizeWidthAnimation(v_top,layoutFullWidth - marginOfLines);
            animation2 = new ResizeWidthAnimation(v_bottom,layoutFullWidth - marginOfLines);
        }else {
            animation = new ResizeWidthAnimation(v_top,0);
            animation2 = new ResizeWidthAnimation(v_bottom,0);
        }
        animation.setDuration(200);
        animation2.setDuration(200);
        v_top.startAnimation(animation);
        v_bottom.startAnimation(animation2);
    }

    private void toggleAddRemove(boolean isRemove) {
        if(isRemove){
            ObjectAnimator oa2= ObjectAnimator.ofFloat(iv_image, "rotation", 45);
            oa2.setDuration(200);
            oa2.start();
        }else {
            ObjectAnimator  oa2= ObjectAnimator.ofFloat(iv_image, "rotation", 0);
            oa2.setDuration(200);
            oa2.start();
        }
    }

}
